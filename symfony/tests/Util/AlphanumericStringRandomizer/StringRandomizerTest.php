<?php

namespace App\Tests\Util\AlphanumericStringRandomizer;

use App\Util\AlphanumericStringRandomizer\StringRandomizer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class StringRandomizerTest
 * @package App\Tests\Util\AlphanumericStringRandomizer
 */
class StringRandomizerTest extends KernelTestCase
{
    public function testRandomize()
    {
        $negativeLengthScenario = -2;
        $this->assertEquals('', StringRandomizer::randomize($negativeLengthScenario));
        $zeroLengthScenario = 0;
        $this->assertEquals('', StringRandomizer::randomize($zeroLengthScenario));
        $naturalLengthScenario = 2;
        $this->assertIsString(StringRandomizer::randomize($naturalLengthScenario));
        $this->assertGreaterThan(0, strlen(StringRandomizer::randomize($naturalLengthScenario)));
    }
}