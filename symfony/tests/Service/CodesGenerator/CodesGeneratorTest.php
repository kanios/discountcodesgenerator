<?php

namespace App\Tests\Service\CodesGenerator\CodesGeneratorTest;

use App\Exception\NegativeParameterException;
use App\Service\CodesGenerator\CodesGenerator;
use App\Service\CodesGenerator\Command\Generator;
use App\ValueObject\CodesGenerator\CodeCount;
use App\ValueObject\CodesGenerator\CodeFilename;
use App\ValueObject\CodesGenerator\CodeLength;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class CodesGeneratorTest
 * @package App\Tests\Service\CodesGenerator\CodesGeneratorTest
 */
class CodesGeneratorTest extends KernelTestCase
{
    /**
     * @throws NegativeParameterException
     */
    public function testHandleWrongParameters()
    {
        $count = new CodeCount(100);
        $length = new CodeLength(1);
        $codesGenerator = new CodesGenerator(new Generator());
        $result = $codesGenerator->generate($count, $length);
        $this->assertArrayHasKey('success', $result);
        $this->assertFalse($result['success']);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals($result['message'], CodesGenerator::TO_MANY_ERROR_MSG);
    }

    /**
     * @throws NegativeParameterException
     */
    public function testHandleNegativeParameters()
    {
        $codesGenerator = new CodesGenerator(new Generator());
        $this->expectException(NegativeParameterException::class);
        $codesGenerator->generate(new CodeCount(-5), new CodeLength(0));
    }

    /**
     * @throws NegativeParameterException
     */
    public function testHandleWithProperParams()
    {
        $codesGenerator = new CodesGenerator(new Generator());
        $result = $codesGenerator->generate(new CodeCount(15), new CodeLength(10));
        $this->assertArrayHasKey('success', $result);
        $this->assertTrue($result['success']);
        $this->assertArrayHasKey('filepath', $result);
        $this->assertEquals($result['filepath'], new CodeFilename(null));
        $this->assertFileExists($result['filepath']);
    }
}
