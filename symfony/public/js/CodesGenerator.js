class CodesGenerator {

    static request() {
        $.loader.open();
        $('.btn-dload').attr('disabled', true);
        $.ajax({
            method: 'post',
            url: '/generate',
            data: {
                count: $('#codes_params_count').val(),
                length: $('#codes_params_length').val()
            }
        }).done(function (response) {
            $.loader.close();
            if (response.success) {
                $('.btn-dload').attr('disabled', false);
                $('.btn-dload').click(function() {
                    window.location.href = '/download';
                });
            } else {
                alert(response.message)
            }
        });
    }
}
