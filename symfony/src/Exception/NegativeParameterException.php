<?php

namespace App\Exception;

use Exception;

/**
 * Class NegativeParameterException
 * @package App\Exception
 */
class NegativeParameterException extends Exception
{
    const MESSAGE = "Parameter must be non-negative integer!";

    /**
     * NegativeParameterException constructor.
     */
    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
