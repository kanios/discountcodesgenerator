<?php

namespace App\Constants;

/**
 * Class GenerateCodesCommandConstants
 * @package App\Constants
 */
class GenerateCodesCommandConstants
{
    const DESCRIPTION = 'Command that generates discount codes.';
    const HELP = 'Please remind arguments numberOfCodes and lengthOfCodes are required! Example: codes:generate 10 10';
    const IO_TITLE = 'Discount Codes Generator';
    const NUMBER_OF_CODES_DESC = 'Number of codes you want to generate';
    const LENGTH_OF_CODES_DESC = 'Length of codes you want to generate';
    const FILE_DESC = 'Path of file you want the codes to be saved in';
}
