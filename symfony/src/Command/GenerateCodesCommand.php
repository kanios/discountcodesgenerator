<?php

namespace App\Command;

use App\Constants\GenerateCodesCommandConstants;
use App\Service\CodesGenerator\CodesGeneratorInterface;
use App\ValueObject\CodesGenerator\CodeCount;
use App\ValueObject\CodesGenerator\CodeFilename;
use App\ValueObject\CodesGenerator\CodeLength;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

/**
 * Class GenerateCodesCommand
 * @package App\Command
 */
class GenerateCodesCommand extends Command
{
    /**
     * @var CodesGeneratorInterface
     */
    private $codesGenerator;

    /**
     * GenerateCodesCommand constructor.
     * @param CodesGeneratorInterface $codesGenerator
     */
    public function __construct(CodesGeneratorInterface $codesGenerator)
    {
        $this->codesGenerator = $codesGenerator;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('codes:generate')
            ->setDescription(GenerateCodesCommandConstants::DESCRIPTION)
            ->setHelp(GenerateCodesCommandConstants::HELP)
            ->addArgument(
                'numberOfCodes',
                InputArgument::REQUIRED,
                GenerateCodesCommandConstants::NUMBER_OF_CODES_DESC
            )
            ->addArgument(
                'lengthOfCode',
                InputArgument::REQUIRED,
                GenerateCodesCommandConstants::LENGTH_OF_CODES_DESC
            )
            ->addArgument(
                'file',
                InputArgument::OPTIONAL,
                GenerateCodesCommandConstants::FILE_DESC
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);
        $io->title(GenerateCodesCommandConstants::IO_TITLE);
        try {
            $result = $this->codesGenerator->generate(
                new CodeCount($input->getArgument('numberOfCodes')),
                new CodeLength($input->getArgument('lengthOfCode')),
                new CodeFilename($input->getArgument('file'))
            );
            if ($result['success']) {
                $io->success($result['filepath']);
            } else {
                $io->error($result['message']);
            }
        } catch (Throwable $throwable) {
            $io->error($throwable->getMessage());
        }
    }
}
