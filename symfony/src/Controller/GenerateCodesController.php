<?php

namespace App\Controller;

use App\Form\CodesParamsType;
use App\Service\CodesGenerator\CodesGeneratorInterface;
use App\ValueObject\CodesGenerator\CodeCount;
use App\ValueObject\CodesGenerator\CodeFilename;
use App\ValueObject\CodesGenerator\CodeLength;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

/**
 * Class GenerateCodesController
 * @package App\Controller
 */
class GenerateCodesController extends AbstractController
{
    const ERROR_NULL_VALUES = 'Null values in request!';

    /**
     * @var CodesGeneratorInterface
     */
    private $codesGenerator;

    /**
     * GenerateCodesController constructor.
     * @param CodesGeneratorInterface $codesGenerator
     */
    public function __construct(CodesGeneratorInterface $codesGenerator)
    {
        $this->codesGenerator = $codesGenerator;
    }

    /**
     * @return Response
     * @Route("/", name="generate_codes_view")
     */
    public function indexAction(): Response
    {
        $formView = $this->createForm(CodesParamsType::class)->createView();
        return $this->render('home.html.twig', ['form' => $formView]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/generate", name="generate_codes", methods={"POST"})
     */
    public function generateAction(Request $request): Response
    {
        $params = $request->request->all();
        if (in_array(null, $params)) {
            return new JsonResponse(['success' => false, 'message' => self::ERROR_NULL_VALUES]);
        }
        try {
            $result = $this->codesGenerator->generate(
                new CodeCount(intval($params['count'])),
                new CodeLength(intval($params['length']))
            );
        } catch (Throwable $throwable) {
            return new JsonResponse(['success' => false, 'message' => $throwable->getMessage()]);
        }
        return new JsonResponse($result);
    }

    /**
     * @return BinaryFileResponse
     * @Route("/download", name="download_codes")
     */
    public function downloadAction(): BinaryFileResponse
    {
        return new BinaryFileResponse(new CodeFilename(null));
    }
}
