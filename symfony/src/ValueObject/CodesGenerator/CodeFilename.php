<?php

namespace App\ValueObject\CodesGenerator;

/**
 * Class CodeFilename
 * @package App\ValueObject
 */
class CodeFilename
{
    const DEFAULT_PATH = '/tmp/kody.txt';

    /**
     * @var string|null
     */
    private $name;

    /**
     * CodeFilename constructor.
     * @param string|null $filename
     */
    public function __construct(?string $filename)
    {
        $this->name = $filename ?: self::DEFAULT_PATH;
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        return $this->name;
    }
}
