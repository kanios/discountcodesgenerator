<?php

namespace App\ValueObject\CodesGenerator;

use App\Exception\NegativeParameterException;

/**
 * Class AbstractCodeParameter
 * @package App\ValueObject
 */
abstract class AbstractCodeParameter
{
    /**
     * @var int
     */
    protected $value;

    /**
     * AbstractCodeParameter constructor.
     * @param int $value
     * @throws NegativeParameterException
     */
    public function __construct(int $value)
    {
        $this->value = $value;
        $this->validate();
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @throws NegativeParameterException
     */
    protected function validate()
    {
        if ($this->value < 0) {
            throw new NegativeParameterException();
        }
    }
}
