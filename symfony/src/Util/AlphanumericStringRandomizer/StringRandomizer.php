<?php

namespace App\Util\AlphanumericStringRandomizer;

/**
 * Class StringRandomizer
 * @package App\Util\AlphanumericStringRandomizer
 */
class StringRandomizer implements StringRandomizerInterface
{
    const PERMITTED_CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    /**
     * @param int $length
     * @return string
     */
    public static function randomize(int $length): string
    {
        $inputLength = strlen(self::PERMITTED_CHARS);
        $randomString = '';
        for($i = 0; $i < $length; $i++) {
            $randomCharacter = self::PERMITTED_CHARS[mt_rand(0, $inputLength - 1)];
            $randomString .= $randomCharacter;
        }
        return $randomString;
    }
}
