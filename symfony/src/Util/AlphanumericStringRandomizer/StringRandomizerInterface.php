<?php

namespace App\Util\AlphanumericStringRandomizer;

interface StringRandomizerInterface
{
    public static function randomize(int $length): string;
}
