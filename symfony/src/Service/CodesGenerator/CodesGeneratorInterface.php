<?php

namespace App\Service\CodesGenerator;

use App\ValueObject\CodesGenerator\CodeCount;
use App\ValueObject\CodesGenerator\CodeFilename;
use App\ValueObject\CodesGenerator\CodeLength;

/**
 * Interface CodesGeneratorInterface
 * @package App\Service\CodesGenerator
 */
interface CodesGeneratorInterface
{
    /**
     * @param CodeCount $count
     * @param CodeLength $length
     * @param CodeFilename|null $filename
     * @return array
     */
    public function generate(CodeCount $count, CodeLength $length, ?CodeFilename $filename = null): array;
}
