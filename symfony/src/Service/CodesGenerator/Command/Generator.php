<?php

namespace App\Service\CodesGenerator\Command;

use App\Util\AlphanumericStringRandomizer\StringRandomizer;
use App\ValueObject\CodesGenerator\CodeCount;
use App\ValueObject\CodesGenerator\CodeFilename;
use App\ValueObject\CodesGenerator\CodeLength;

/**
 * Class Generator
 * @package App\Service\CodesGenerator\Command
 */
class Generator implements GeneratorInterface
{
    /**
     * @param CodeCount $codeCount
     * @param CodeLength $codeLength
     * @param CodeFilename $filename
     * @return string
     */
    public function handle(CodeCount $codeCount, CodeLength $codeLength, CodeFilename $filename): string
    {
        $codes = '';
        $counter = 0;
        while ($counter < $codeCount->getValue() - 1) {
            $code = $this->randomize($codeLength);
            if (strpos($codes, $code) === false) {
                $codes .= $code. "\n";
                $counter++;
            }
        }
        file_put_contents($filename, $codes);
        return $filename;
    }

    /**
     * @param CodeLength $codeLength
     * @return string
     */
    private function randomize(CodeLength $codeLength): string
    {
        return StringRandomizer::randomize($codeLength->getValue());
    }
}
