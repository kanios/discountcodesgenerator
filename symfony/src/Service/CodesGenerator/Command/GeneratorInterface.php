<?php

namespace App\Service\CodesGenerator\Command;

use App\ValueObject\CodesGenerator\CodeCount;
use App\ValueObject\CodesGenerator\CodeFilename;
use App\ValueObject\CodesGenerator\CodeLength;

/**
 * Interface GeneratorInterface
 * @package App\Service\CodesGenerator\Command
 */
interface GeneratorInterface
{
    /**
     * @param CodeCount $codeCount
     * @param CodeLength $codeLength
     * @param CodeFilename $filename
     * @return string
     */
    public function handle(CodeCount $codeCount, CodeLength $codeLength, CodeFilename $filename): string;
}
