<?php

namespace App\Service\CodesGenerator;

use App\Service\CodesGenerator\Command\GeneratorInterface;
use App\Util\AlphanumericStringRandomizer\StringRandomizer;
use App\ValueObject\CodesGenerator\CodeCount;
use App\ValueObject\CodesGenerator\CodeFilename;
use App\ValueObject\CodesGenerator\CodeLength;
use Throwable;

/**
 * Class CodesGenerator
 * @package App\Service\CodesGenerator
 */
class CodesGenerator implements CodesGeneratorInterface
{
    const TO_MANY_ERROR_MSG =
        'It is impossible to generate that huge amount of codes of given value! Fix your request and try again.'
    ;

    /**
     * @var GeneratorInterface
     */
    private $generator;

    /**
     * CodesGenerator constructor.
     * @param GeneratorInterface $generator
     */
    public function __construct(GeneratorInterface $generator)
    {
        $this->generator = $generator;
    }

    /**
     * @param CodeCount $count
     * @param CodeLength $length
     * @param CodeFilename|null $filename
     * @return array
     */
    public function generate(CodeCount $count, CodeLength $length, ?CodeFilename $filename = null): array
    {
        if ($this->checkParams($count, $length)) {
            return ['success' => false, 'message' => self::TO_MANY_ERROR_MSG];
        }
        try {
            $filename = $filename ?: new CodeFilename(null);
            $resultFile = $this->generator->handle($count, $length, $filename);
        } catch (Throwable $throwable) {
            return ['success' => false, 'message' => $throwable->getMessage()];
        }
        return ['success' => true, 'filepath' => $resultFile];
    }

    private function checkParams(CodeCount $count, CodeLength $length): bool
    {
        return pow(strlen(StringRandomizer::PERMITTED_CHARS), $length->getValue()) < $count->getValue();
    }
}
