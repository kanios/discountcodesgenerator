**Requirements**:
-
* [Docker](www.docker.com) & [docker-compose](https://docs.docker.com/compose/) is all you need to run the application

**Running the app:**
-
* To start **DiscountCodesGenerator** you want to put `docker-compose up` command to your console

**CLI mode:**
-
* Enter your docker container by typing `docker exec -ti PHP_CONTAINER_ID sh` to your console
* Generate codes with `php bin/console codes:generate NUMBER_OF_CODES LENGTH_OF_CODES FILE_PATH` command
* Please remind that NUMBER_OF_CODES and LENGTH_OF_CODES are supposed to be integers
* FILE_PATH string is optional and it is */tmp/kody.txt* by default

> NOTE: You get **container id**  i.e. by listing your running containers with `docker ps` command


> NOTE2: Please remind that file from console command will be created **inside** your docker container!

**Browser mode:**
-
* Just enter `localhost:8080`
 
**Testing:**
-
* You run tests by putting `php bin/phpunit` into your console inside your PHP container
